package org.uth.aslaug.client.algorithms;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class ProbabilityInverseBroadcactFrequencyStepWidget extends Composite
{
	public ProbabilityInverseBroadcactFrequencyStepWidget(int nStep,
															String broadcastSchedule,
															String clientRequests,
															int nRequested,
															String cacheContent)
	{
		final int broadcastIndex = nStep % broadcastSchedule.length();
		
		VerticalPanel vpnBase = new VerticalPanel();
		vpnBase.setBorderWidth(2);
		
		Label lbl = new Label("Step " + (nStep + 1));
		lbl.setStyleName("step", true);
		vpnBase.add(lbl);		
		
		HorizontalPanel hpn = new HorizontalPanel();
		hpn.setSpacing(5);
		
		lbl = new Label();
		lbl.setText("Broadcast: ");
		hpn.add(lbl);
		
		// broadcasted
		lbl = new Label();
		lbl.setStyleName("passed", true);
		lbl.setText(broadcastSchedule.substring(0, broadcastIndex));
		hpn.add(lbl);

		// current broadcast
		lbl = new Label();
		lbl.setStyleName("current", true);
		lbl.setText(broadcastSchedule.charAt(broadcastIndex) + "");
		hpn.add(lbl);

		// next broadcasts
		lbl = new Label();
		lbl.setText(broadcastSchedule.substring(broadcastIndex + 1, broadcastSchedule.length()));
		hpn.add(lbl);

		vpnBase.add(hpn);
		
		hpn = new HorizontalPanel();
		hpn.setSpacing(5);
		
		// cache contents
		lbl = new Label();
		lbl.setText("Cache: " + cacheContent);	// some extra space 
		hpn.add(lbl);
		
		lbl = new Label();
		lbl.setText("Requests: ");
		hpn.add(lbl);
	
		// requested
		lbl = new Label();
		lbl.setStyleName("passed", true);
		lbl.setText(clientRequests.substring(0, nRequested));
		hpn.add(lbl);

		// current request 
		lbl = new Label();
		lbl.setStyleName("current", true);
		lbl.setText(clientRequests.charAt(nRequested) + "");
		hpn.add(lbl);

		// next requests 
		lbl = new Label();
		lbl.setText(clientRequests.substring(nRequested + 1, clientRequests.length()));
		hpn.add(lbl);
		
		vpnBase.add(hpn);

		initWidget(vpnBase);

		// Give the overall composite a style name.
		setStyleName("probabilityInverseBroadcactFrequencyStepWidget");
	}
}
