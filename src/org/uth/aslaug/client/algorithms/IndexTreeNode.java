package org.uth.aslaug.client.algorithms;

import java.util.ArrayList;
import java.util.List;

public class IndexTreeNode<T extends Comparable<T>> implements Comparable<IndexTreeNode<T>>
{
	private T data;
    private IndexTreeNode<T> parent;
    private List<IndexTreeNode<T>> childrens;
    
    public IndexTreeNode()
    {
    	childrens = new ArrayList<>();
    	parent = null;
    	data = null;
    }
    
    public IndexTreeNode(T data)
    {
    	this();
    	this.data = data;
    }
    
    public void addChildren(IndexTreeNode<T> n)
	{
    	if (n.getParent() != null)
		{
			n.getParent().removeChildren(n);
		}
    	n.setParent(this);
		childrens.add(n);
	}
    
    public boolean removeChildren(IndexTreeNode<T> n)
	{
		boolean b = childrens.remove(n);
		if (b)
		{
			n.setParent(null);
		}
		
		return b;
	}    
    
	public IndexTreeNode<T> removeChildren(int index)
	{
		IndexTreeNode<T> n = childrens.remove(index);
		n.setParent(null);
		
		return n;
	}

	private void setParent(IndexTreeNode<T> parent)
	{
		this.parent = parent;
	}
    
	public void setData(T data)
	{
		this.data = data;
	}
	
    public T getData()
	{
		return data;
	}
    
    public IndexTreeNode<T> getParent()
	{
		return parent;
	}
    
    public List<IndexTreeNode<T>> getChildrensList()
	{
		return childrens;
	}
    
    public int getTotalNodesCount()
	{
		int n = 1;
		
		for (IndexTreeNode<T> itn : childrens)
		{
			n += itn.getTotalNodesCount();
		}
		
		return n;
	}

    @Override
    public String toString()
    {
    	return data.toString();
    }
    
	@Override
	public int compareTo(IndexTreeNode<T> o)
	{
		return getData().compareTo(o.getData());
	}
}
