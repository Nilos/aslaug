package org.uth.aslaug.client.algorithms;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.TextBox;

public class GraphEdgeInputWidget extends Composite
{
	private Panel pnlBase;

	private ListBox lsbFrom;
	private ListBox lsbTo;
	private TextBox txbWeight;
	
	private VisJsGraph graph;
	private VisJsEdge edge;
	
	private int id;

	private final KeyUpHandler kuhEdgeInputs = new KeyUpHandler(){
		// TODO: take a look at
		// http://stackoverflow.com/questions/8036561/allow-only-numbers-in-textbox-in-gwt
		@Override
		public void onKeyUp(KeyUpEvent event)
		{
			onEdgeInputs();
		}
	};	

	private final ChangeHandler cghEdgeInputs = new ChangeHandler(){
		
		@Override
		public void onChange(ChangeEvent event)
		{
			onEdgeInputs();			
		}
	};

	public GraphEdgeInputWidget(VisJsGraph graph)
	{
		this.graph = graph;
		this.id = -1;
		this.edge = null;
		
		pnlBase = new HorizontalPanel();
		((HorizontalPanel) pnlBase).setSpacing(5);

		lsbFrom = new ListBox();
		lsbFrom.setTitle("from");
		lsbFrom.addChangeHandler(cghEdgeInputs);
		pnlBase.add(lsbFrom);
		
		lsbTo = new ListBox();
		lsbTo.setTitle("to");
		lsbTo.addChangeHandler(cghEdgeInputs);
		pnlBase.add(lsbTo);
		
		updateEdges();
			
		String weightTitle = "edge's weight";
		txbWeight = new TextBox();
		txbWeight.setTitle(weightTitle);
		txbWeight.getElement().setPropertyString("placeholder", weightTitle);
		txbWeight.getElement().setPropertyString("type", "number");
		txbWeight.getElement().setPropertyInt("min", 0);
		txbWeight.getElement().setPropertyInt("step", 1);
		txbWeight.addKeyUpHandler(kuhEdgeInputs);
		txbWeight.addChangeHandler(cghEdgeInputs);
		
		pnlBase.add(txbWeight);
		
		initWidget(pnlBase);

		// Give the overall composite a style name.
		setStyleName("graphEdgeInputWidget");
	}
	
	public void updateEdges()
	{
		int from = lsbFrom.getSelectedIndex();
		int to = lsbTo.getSelectedIndex();
		
		int n = graph.getNodesCount();
		lsbFrom.clear();
		lsbTo.clear();
		for (int i = 1; i <= n; i++)
		{
			lsbFrom.addItem(i + "");
			lsbTo.addItem(i + "");
		}

		if (from < n)
		{
			lsbFrom.setSelectedIndex(from);
		}

		if (to < n)
		{
			lsbTo.setSelectedIndex(to);
		}

		if (from >= n || to >= n)
		{
			removeFromGraph();
			txbWeight.setText("");
		}
	}

	public void removeFromGraph()
	{
		graph.removeEdge(id);
		id = -1;
		edge = null;
	}

	public boolean hasValidInput()
	{
		return id >= 0
			&& lsbFrom.getSelectedIndex() >= 0 
			&& lsbTo.getSelectedIndex() >= 0 
			&& Integer.parseInt(txbWeight.getText()) > 0;
	}

	public void setEnabled(boolean enabled)
	{
		lsbFrom.setEnabled(enabled);
		lsbTo.setEnabled(enabled);
		txbWeight.setReadOnly(!enabled);
	}

	private void onEdgeInputs()
	{
		int from = lsbFrom.getSelectedIndex() + 1;
		int to = lsbTo.getSelectedIndex() + 1;
		int weight = Integer.parseInt(txbWeight.getText());

		if (id < 0)
		{
			id = graph.addEdge(from, to, weight);
			edge = VisJsEdge.create(id, from, to, weight);
		}
		else 
		{
			graph.updateEdge(id, from, to, weight);
			edge.update(from, to, weight);
		}
	}
	
	public VisJsEdge getEdge()
	{
		return edge;
	}
}
