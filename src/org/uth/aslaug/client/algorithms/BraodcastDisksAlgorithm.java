package org.uth.aslaug.client.algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import org.uth.aslaug.client.AlgorithmInputsProvider;
import org.uth.aslaug.client.Utils;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class BraodcastDisksAlgorithm extends AbstractBaseAlgorithm
	implements AlgorithmInputsProvider
{
	private final static Logger LOG = Logger.getLogger(BraodcastDisksAlgorithm.class.getName());
	
	private int nPages;	
	/** popularity of each page */
	@SuppressWarnings("unused")
	private double[] popularities;
	
	private int nDisks;
	/** frequency of each disk */
	private long[] frequencies;
	/** index is page id, value is disk id */
	private int[] diskIdOfPage;
	/** index is disk id, value is num_chunks */
	private long[] chunksOfDisk;
	
	private BroadcastDisk[] disks;
	private BroadcastProgram[] programs;
	
	private TextBox txbDisksNum;
	private TextBox txbPagesNum;
	private TextBox[] atxbDiskFrequency;
	private ListBox[] alsbDiskId;
	private Button btnDisksInputDone;
	private Button btnFrequenciesInputDone;

	private void onDisksInputs()
	{
		try
		{
			nPages = Integer.parseInt(txbPagesNum.getText());
			LOG.info("pages: " + nPages);
			
			nDisks = Integer.parseInt(txbDisksNum.getText());
			LOG.info("disks: " + nDisks);
			
			int n = ((ComplexPanel) pnlBase).getWidgetCount();
			for (int i = 1; i < n; i++)
			{
				((ComplexPanel) pnlBase).remove(1);
			}
			
			// building disks inputs
			alsbDiskId = new ListBox[nPages];
			for (int i = 0; i < nPages; i++)
			{
				HorizontalPanel hp = new HorizontalPanel();
				String textLbl = "Disk id of page " + (i + 1) + ":";
				Label lbl = new Label(textLbl);
				
				alsbDiskId[i] = new ListBox();
				for (int j = 1; j <= nDisks; j++)
				{
					alsbDiskId[i].addItem("" + j);
					
				}
				alsbDiskId[i].setSelectedIndex(i % nDisks);	// just for fun :p
				hp.add(lbl);
				hp.add(alsbDiskId[i]);
				
				pnlBase.add(hp);
			}
			
			String disksInputDoneText = "Done";
			btnDisksInputDone = new Button(disksInputDoneText);
			btnDisksInputDone.addClickHandler(chDisksInputDone);
			pnlBase.add(btnDisksInputDone);
		}
		catch (NumberFormatException e)
		{
			LOG.info("pages: input error");
		}
	}
	
	private final KeyUpHandler kuhDisksInputs = new KeyUpHandler(){
		// TODO: take a look at
		// http://stackoverflow.com/questions/8036561/allow-only-numbers-in-textbox-in-gwt
		@Override
		public void onKeyUp(KeyUpEvent event)
		{
			onDisksInputs();
		}
	};	
	
	private final ChangeHandler cghDisksInputs = new ChangeHandler(){
		
		@Override
		public void onChange(ChangeEvent event)
		{
			onDisksInputs();			
		}
	};
	
	private final ClickHandler chDisksInputDone = new ClickHandler(){
		
		@Override
		public void onClick(ClickEvent event)
		{
			String disksInputDoneText = "Reset";
			
			if (disksInputDoneText.equals(btnDisksInputDone.getText()))
			{
				LOG.info("disk inputs reset");
				
				getInputWidget();
			}
			else
			{
				LOG.info("disk inputs done");
				
				txbPagesNum.setReadOnly(true);
				txbDisksNum.setReadOnly(true);
				diskIdOfPage = new int[nPages];
				for (int i = 0; i < nPages; i++)
				{
					alsbDiskId[i].setEnabled(false);
					diskIdOfPage[i] = -1 + Integer.parseInt(
							alsbDiskId[i].getValue(alsbDiskId[i].getSelectedIndex()));
				}
				
				disks = new BroadcastDisk[nDisks];
				for (int i = 0; i < nDisks; i++)
				{
					disks[i] = new BroadcastDisk();
					
					for (int j = 0; j < nPages; j++)
					{
						if (diskIdOfPage[j] == i)
						{
							disks[i].addPage(j);
						}
					}
				}

				btnDisksInputDone.setText(disksInputDoneText);
				
				String title = "frequency of disk ";
				atxbDiskFrequency = new TextBox[nDisks];
				for (int i = 0; i < nDisks; i++)
				{
					atxbDiskFrequency[i] = new TextBox();
					atxbDiskFrequency[i].setTitle(title + (i + 1));
					atxbDiskFrequency[i].getElement().setPropertyString("placeholder", title + (i + 1));
					atxbDiskFrequency[i].getElement().setPropertyString("type", "number");
					atxbDiskFrequency[i].getElement().setPropertyInt("min", 0);
					atxbDiskFrequency[i].getElement().setPropertyInt("step", 1);
					
					pnlBase.add(atxbDiskFrequency[i]);
				}
				
				String frequenciesInputDoneText = "Done";				
				btnFrequenciesInputDone = new Button(frequenciesInputDoneText);
				btnFrequenciesInputDone.addClickHandler(chFrequenciesInputDone);
				pnlBase.add(btnFrequenciesInputDone);
			}
		}
	};
	
	@SuppressWarnings("unused")
	private final KeyUpHandler kuhFrequenciesInputs = new KeyUpHandler(){

		@Override
		public void onKeyUp(KeyUpEvent event)
		{
			// TODO Auto-generated method stub
			
		}
	};
	
	private final ClickHandler chFrequenciesInputDone = new ClickHandler(){

		@Override
		public void onClick(ClickEvent event)
		{
			frequencies = new long[nDisks];
			int i = 0;
			try
			{
				for (i = 0; i < nDisks; i++)
				{
					frequencies[i] = Integer.parseInt(atxbDiskFrequency[i].getText());
				}
				
				LOG.info("frequencies done");
				
				long lcm = Utils.lcm(frequencies);
				
				Label lblLcm = new Label("Lowest Common Multiple of " + Arrays.toString(frequencies) + " is " + lcm);
				pnlBase.add(lblLcm);
				
				chunksOfDisk = new long[nDisks];
				for (int j = 0; j < nDisks; j++)
				{
					chunksOfDisk[j] = lcm / frequencies[j];
					disks[j].splitInChunks((int) chunksOfDisk[j]);
					
					LOG.info("disk " + (j + 1) + " chunks " + chunksOfDisk[j]);
					for (int k = 0; k < chunksOfDisk[j]; k++)
					{
						LOG.info("\t" + k + " " + Arrays.toString(disks[j].getChunks()[k].toArray()));
					}
				}
				
				programs = new BroadcastProgram[(int) lcm];
				for (int j = 0; j < lcm; j++)
				{
					programs[j] = new BroadcastProgram();
					
					for (int k = 0; k < nDisks; k++)
					{
						for (Integer pageId : disks[k].getChunks()[j % (int) chunksOfDisk[k]])
						{
							programs[j].addPage(pageId, k);
						}
					}
					
					LOG.info(j + " broadcast " + Arrays.toString(programs[j].getPages().toArray()));
				}
				
				HorizontalPanel hpn = new HorizontalPanel();
				hpn.setSpacing(20);

				for (BroadcastProgram program : programs)
				{
					BroadcastProgramWidget bpw = new BroadcastProgramWidget();
					
					int n = program.getPages().size();
					for (int j = 0; j < n; j++)
					{
						bpw.addChunk(program.getDiskIdOfPage().get(j) + 1, program.getPages().get(j) + 1);
					}
					
					hpn.add(bpw);
				}
				
				ScrollPanel scp = new ScrollPanel(hpn);
				scp.setWidth("450px");
				
				pnlBase.add(scp);
				
				for (int j = 0; j < nDisks; j++)
				{
					atxbDiskFrequency[j].setEnabled(false);
				}
				pnlBase.remove(btnFrequenciesInputDone);
				
				pnlBase.remove(btnDisksInputDone);
				pnlBase.add(btnDisksInputDone);
			}
			catch (NumberFormatException e)
			{
				LOG.info("input error for frequency " + i);
			}
		}		
	};

	@Override
	public IsWidget getInputWidget()
	{
		if (pnlBase == null)
		{
			pnlBase = new VerticalPanel();
			((VerticalPanel) pnlBase).setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
			((VerticalPanel) pnlBase).setSpacing(3);
		}
		pnlBase.clear();
		String pagesTitle = "total nubmer of pages";
		String disksTitle = "total nubmer of disks";
		
		txbDisksNum = new TextBox();
		txbDisksNum.setTitle(disksTitle);
		txbDisksNum.getElement().setPropertyString("placeholder", disksTitle);
		txbDisksNum.getElement().setPropertyString("type", "number");
		txbDisksNum.getElement().setPropertyInt("min", 0);
		txbDisksNum.getElement().setPropertyInt("step", 1);
		txbDisksNum.addKeyUpHandler(kuhDisksInputs);
		txbDisksNum.addChangeHandler(cghDisksInputs);
		
		txbPagesNum = new TextBox();
		txbPagesNum.setTitle(pagesTitle);
		txbPagesNum.getElement().setPropertyString("placeholder", pagesTitle);
		txbPagesNum.getElement().setPropertyString("type", "number");
		txbPagesNum.getElement().setPropertyInt("min", 0);
		txbPagesNum.getElement().setPropertyInt("step", 1);
		txbPagesNum.addKeyUpHandler(kuhDisksInputs);
		txbPagesNum.addChangeHandler(cghDisksInputs);
		
		HorizontalPanel hp = new HorizontalPanel();
		hp.setSpacing(3);
		hp.add(txbPagesNum);
		hp.add(txbDisksNum);
		
		pnlBase.add(hp);

		return pnlBase;
	}
	
	private class BroadcastDisk
	{
		private List<Integer> pages;
		private List<Integer>[] chunks;
		
		public BroadcastDisk()
		{
			this.pages = new ArrayList<>();
		}
		
		public void addPage(int pageId)
		{
			this.pages.add(pageId);
		}
		
		@SuppressWarnings("unchecked")
		public void splitInChunks(int nChunks)
		{
			this.chunks = new List[nChunks];
			for (int i = 0; i < nChunks; i++)
			{
				this.chunks[i] = new ArrayList<>();				
			}
			
			int n = this.pages.size() % nChunks;
			if (n > 0)
			{
				n = nChunks - n;
			}
			for (int i = 0; i < n; i++)
			{
				addPage(-1);	// empty page
			}
			
			n = this.pages.size();
			for (int i = 0; i < n; i++)
			{
				this.chunks[i % nChunks].add(this.pages.get(i));
			}
		}
		
		@SuppressWarnings("unused")
		public List<Integer> getPages()
		{
			return this.pages;
		}
		
		public List<Integer>[] getChunks()
		{
			return this.chunks;
		}
	}
	
	private class BroadcastProgram
	{
		private List<Integer> pages;
		private List<Integer> diskIdOfPage;
		
		public BroadcastProgram()
		{
			this.pages = new ArrayList<>();
			this.diskIdOfPage = new ArrayList<>();
		}
		
		public void addPage(int pageId, int diskId)
		{
			this.pages.add(pageId);
			this.diskIdOfPage.add(diskId);
		}
		
		public List<Integer> getPages()
		{
			return this.pages;
		}
		
		public List<Integer> getDiskIdOfPage()
		{
			return this.diskIdOfPage;
		}
	}
}
