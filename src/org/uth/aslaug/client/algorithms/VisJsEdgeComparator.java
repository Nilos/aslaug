package org.uth.aslaug.client.algorithms;

import java.util.Comparator;

public class VisJsEdgeComparator implements Comparator<VisJsEdge>
{
	@Override
	public int compare(VisJsEdge t, VisJsEdge o)
	{
		if (t.getWeight() > o.getWeight())
		{
			return 1;
		}

		if (t.getWeight() < o.getWeight())
		{
			return -1;
		}

		return 0;
	}
}
