package org.uth.aslaug.client.algorithms;

import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;

public class TreeNodeInputWidget extends Composite
{
	private int id;
	
	private HorizontalPanel pnlBase;
	private TextBox txbProbability;
	
	public TreeNodeInputWidget(int id)
	{
		this.id = id;
		
		pnlBase = new HorizontalPanel();
		pnlBase.setVerticalAlignment(HasVerticalAlignment.ALIGN_BOTTOM);
		pnlBase.setSpacing(5);
		
		String nameText = "N" + this.id;
		Label lblName = new Label(nameText);
		pnlBase.add(lblName);
		
		String probabilityTitle = "";
		txbProbability = new TextBox();
		txbProbability.setText("0");
		txbProbability.setTitle(probabilityTitle);
		txbProbability.getElement().setPropertyString("placeholder", probabilityTitle);
		txbProbability.getElement().setPropertyString("type", "number");
		txbProbability.getElement().setPropertyInt("min", 0);
		txbProbability.getElement().setPropertyDouble("step", .1);
		pnlBase.add(txbProbability);
		
		initWidget(pnlBase);

		// Give the overall composite a style name.
		setStyleName("treeNodeInputWidget");
	}	
	
	public HandlerRegistration addChangeHandler(ChangeHandler handler)
	{
		return txbProbability.addChangeHandler(handler);
	}

	public HandlerRegistration addKeyUpHandler(KeyUpHandler handler)
	{
		return txbProbability.addKeyUpHandler(handler);
	}

	public double getProbability()
	{
		return Double.parseDouble(txbProbability.getText());
	}
}
