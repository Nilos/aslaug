package org.uth.aslaug.client.algorithms;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;


public class VisJsGraph
{
	private JavaScriptObject nodes;
	private JavaScriptObject edges;
	private JavaScriptObject graph;
	
	public VisJsGraph(int nodes)
	{
		initJs();
		setNodes(nodes);
	}
	
	public native void initJs()
	/*-{
		this.@org.uth.aslaug.client.algorithms.VisJsGraph::nodes = new $wnd.vis.DataSet();		
		this.@org.uth.aslaug.client.algorithms.VisJsGraph::edges = new $wnd.vis.DataSet();
		
		var container = $doc.getElementById('graph');
		var data = {
			nodes: this.@org.uth.aslaug.client.algorithms.VisJsGraph::nodes,
			edges: this.@org.uth.aslaug.client.algorithms.VisJsGraph::edges
		};
		
		var options = {};
		this.@org.uth.aslaug.client.algorithms.VisJsGraph::graph = new $wnd.vis.Network(container, data, options);
	}-*/;
	
	public native int addEdge(int fromId, int toId, int weight)
	/*-{
		var edges = this.@org.uth.aslaug.client.algorithms.VisJsGraph::edges;
		var eid = edges.length + 1;
		edges.add([{id: eid, from: fromId, to: toId, label: weight, arrows: 'to'}]);
		
		return eid;
	}-*/;
	
	public native int addEdge(int fromId, int toId)
	/*-{
		var edges = this.@org.uth.aslaug.client.algorithms.VisJsGraph::edges;
		var eid = edges.length + 1;
		edges.add([{id: eid, from: fromId, to: toId, arrows: 'to'}]);
		
		return eid;
	}-*/;
	
	public native int addNode(String label)
	/*-{
		var nodes = this.@org.uth.aslaug.client.algorithms.VisJsGraph::nodes;
		var nid = nodes.length + 1;
		nodes.add([{id: nid, label: label}]);
		
		return nid;
	}-*/;
	
	/**
	 * Removes the last added edge
	 */
	public native void removeEdge()
	/*-{
		var edges = this.@org.uth.aslaug.client.algorithms.VisJsGraph::edges;		
		edges.remove(edges.length);
	}-*/;
	
	/**
	 * Removes the edge with specific id
	 */
	public native void removeEdge(int id)
	/*-{
		var edges = this.@org.uth.aslaug.client.algorithms.VisJsGraph::edges;		
		edges.remove(id);
	}-*/;
	
	public native int getEdgesCount()
	/*-{
		return this.@org.uth.aslaug.client.algorithms.VisJsGraph::edges.length;
	}-*/;
	
	public native void clearEdges()
	/*-{
		this.@org.uth.aslaug.client.algorithms.VisJsGraph::edges.clear();
	}-*/;
	
	public native void updateEdge(int edgeId, int fromId, int toId, int weight)
	/*-{
		var edges = this.@org.uth.aslaug.client.algorithms.VisJsGraph::edges;
		var d = {id: edgeId, from: fromId, to: toId, label: weight};
		edges.update([d]);
	}-*/;
	
	public native int getNodesCount()
	/*-{
		return this.@org.uth.aslaug.client.algorithms.VisJsGraph::nodes.length;
	}-*/;
	
	public native JsArray<VisJsEdge> getEdges()
	/*-{
		var edges = this.@org.uth.aslaug.client.algorithms.VisJsGraph::edges;
		var aEdges = [];

		$wnd.console.log('forEach');
		
		edges.forEach(function(e)
				{
					$wnd.console.log('in forEach');
					aEdges.push(
						@org.uth.aslaug.client.algorithms.VisJsEdge::create(IIII)(e.id, e.from, e.to, e.label)
					);
				}, 
			{type: {'label': 'Number'}, order: 'label'}
		);

		return aEdges;
	}-*/;

	public native void setNodes(int nNodes)
	/*-{
		var nodes = this.@org.uth.aslaug.client.algorithms.VisJsGraph::nodes;

		if (nNodes < nodes.length)
		{
			nodes.clear();
		}
		
		var aNodes = [];

		for (i = nodes.length + 1; i <= nNodes; i++) 
		{
			aNodes.push({id: i, label: 'Node ' + i});
		}
		
		nodes.add(aNodes);
		
		this.@org.uth.aslaug.client.algorithms.VisJsGraph::graph.fit();
	}-*/;
	
	public native void fitGraph(boolean asTree)
	/*-{
		if (asTree)
		{
			this.@org.uth.aslaug.client.algorithms.VisJsGraph::graph.setOptions(
				{
					layout:
						{
							hierarchical: 
								{
									enabled: true,
									sortMethod: 'directed'
								}
						}
				}
			);
		}
		
		this.@org.uth.aslaug.client.algorithms.VisJsGraph::graph.fit();
	}-*/;
	
	public native void highlightEdge(int id)
	/*-{
		var edges = this.@org.uth.aslaug.client.algorithms.VisJsGraph::edges;
		
		edges.update([{id: id, color: {color: 'red', highlight: 'red'}}]);
	}-*/;
}
