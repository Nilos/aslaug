package org.uth.aslaug.client.algorithms;

import org.uth.aslaug.client.Utils;
import com.google.gwt.canvas.dom.client.CssColor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;

public class RXWInputItemWidget extends Composite
{
	private int id;
	
	private HorizontalPanel pnlBase;
	
	private TextBox txbRequests;
	private TextBox txbWaitTime;
	
	private TextBox txbMaxRequestsPerTick;
	
	public RXWInputItemWidget(int id)
	{
		this.id = id;
		
		CssColor color = Utils.getUniqueColor(this.id);
		
		pnlBase = new HorizontalPanel();
		pnlBase.setVerticalAlignment(HasVerticalAlignment.ALIGN_BOTTOM);
		pnlBase.setSpacing(5);
		pnlBase.getElement().getStyle().setBackgroundColor(color.value());
		
		String nameText = "Disk #" + this.id;
		Label lblName = new Label(nameText);
		pnlBase.add(lblName);
		
		String requestsTitle = "remaining requests [R]";
		txbRequests = new TextBox();
		txbRequests.setText("0");
		txbRequests.setTitle(requestsTitle);
		txbRequests.getElement().setPropertyString("placeholder", requestsTitle);
		txbRequests.getElement().setPropertyString("type", "number");
		txbRequests.getElement().setPropertyInt("min", 0);
		txbRequests.getElement().setPropertyInt("step", 1);
		pnlBase.add(txbRequests);
		
		String popularityTitle = "max waiting time [W]";
		txbWaitTime = new TextBox();
		txbWaitTime.setTitle(popularityTitle);
		txbWaitTime.getElement().setPropertyString("placeholder", popularityTitle);
		txbWaitTime.getElement().setPropertyString("type", "number");
		txbWaitTime.getElement().setPropertyInt("min", 0);
		txbWaitTime.getElement().setPropertyInt("step", 1);
		pnlBase.add(txbWaitTime);
		
		String reqPerTickTitle = "(optional) max requests per tick";
		txbMaxRequestsPerTick = new TextBox();
		txbMaxRequestsPerTick.setText("0");
		txbMaxRequestsPerTick.setTitle(reqPerTickTitle);
		txbMaxRequestsPerTick.getElement().setPropertyString("placeholder", reqPerTickTitle);
		txbMaxRequestsPerTick.getElement().setPropertyString("type", "number");
		txbMaxRequestsPerTick.getElement().setPropertyInt("min", 0);
		txbMaxRequestsPerTick.getElement().setPropertyInt("step", 1);
		pnlBase.add(txbMaxRequestsPerTick);
		
		initWidget(pnlBase);

		// Give the overall composite a style name.
		setStyleName("rXWInputItemWidget");
	}

	public void setReadOnly(boolean readOnly)
	{
		txbRequests.setReadOnly(readOnly);
		txbWaitTime.setReadOnly(readOnly);
		txbMaxRequestsPerTick.setReadOnly(readOnly);
	}
	
	public int getRequests()
	{
		return Integer.parseInt(txbRequests.getText());
	}
	
	public int getWaitTime()
	{
		return Integer.parseInt(txbWaitTime.getText());
	}
	
	public int getMaxRequestsPerTick()
	{
		return Integer.parseInt(txbMaxRequestsPerTick.getText());
	}
}
