package org.uth.aslaug.client.algorithms;

import static org.uth.aslaug.client.Utils.arrayHas;
import static org.uth.aslaug.client.Utils.arrayMinAndIndexOfMin;
import static org.uth.aslaug.client.Utils.asHTMLFraction;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class ProbabilityInverseBroadcactFrequencyAlgorithm extends AbstractBaseAlgorithm
{
	private final static Logger LOG = Logger.getLogger(ProbabilityInverseBroadcactFrequencyAlgorithm.class.getName());

	private TextBox txbServerBroadcastSchedule;
	private TextBox txbClientRequests;
	private TextBox txbCacheSlots;
	
	private final KeyUpHandler kuhNodesInputs = new KeyUpHandler(){
		// TODO: take a look at
		// http://stackoverflow.com/questions/8036561/allow-only-numbers-in-textbox-in-gwt
		@Override
		public void onKeyUp(KeyUpEvent event)
		{
			onNodesInputs();
		}
	};	
	
	private final ChangeHandler cghNodesInputs = new ChangeHandler(){
		
		@Override
		public void onChange(ChangeEvent event)
		{
			onNodesInputs();			
		}
	};
	
	private void onNodesInputs()
	{
		String broadcastSchedule = txbServerBroadcastSchedule.getText();
		String clientRequests = txbClientRequests.getText();
		int cacheSlots = Integer.parseInt(txbCacheSlots.getText());
		
		//
		// TODO add code to check if all characters of clientRequests 
		// exists on broadcastSchedule and show appropriate message 
		//
		
		while (((ComplexPanel) pnlBase).getWidgetCount() > 4)
		{
			((ComplexPanel) pnlBase).remove(4);
		}
		
		Map<Character, Integer> accessFreq = new HashMap<>();
		for (char c : clientRequests.toCharArray())
		{ // counting the occurrences of each character
			Integer n = accessFreq.get(c);
			if (n == null)
			{
				n = 1;
			}
			else
			{
				n += 1;
			}
			accessFreq.put(c, n);
		}
		
		Map<Character, Integer> broadcastFreq = new HashMap<>();
		for (char c : broadcastSchedule.toCharArray())
		{ // counting the occurrences of each character
			Integer n = broadcastFreq.get(c);
			if (n == null)
			{
				n = 1;
			}
			else
			{
				n += 1;
			}
			broadcastFreq.put(c, n);
		}
		
		final int requests = clientRequests.length();
		final int broadcastLen = broadcastSchedule.length();
		Map<Character, Double> pix = new HashMap<>();
		for (char c : accessFreq.keySet())
		{
			
			double d = (accessFreq.get(c) / (double) requests) / (broadcastFreq.get(c) / (double) broadcastLen);
			String s = "PIX(" + c + ") = " + asHTMLFraction(accessFreq.get(c), requests)
						+ " / " + asHTMLFraction(broadcastFreq.get(c), broadcastLen) + " = " + d;
			Label lbl = new HTML(s);
			pnlBase.add(lbl);
			
			pix.put(c, d);
		}
		
		int step = 0;
		int nRequested = 0;
		char[] cache = new char[cacheSlots];
		double[] cachePIX = new double[cacheSlots];
		
		Arrays.fill(cache, '_');
		Arrays.fill(cachePIX, 0.);
		
		while (nRequested < requests)
		{
			pnlBase.add(new ProbabilityInverseBroadcactFrequencyStepWidget(step, 
							broadcastSchedule, 
							clientRequests, 
							nRequested, 
							new String(cache)));
			
			char broadcasting = broadcastSchedule.charAt(step % broadcastLen);
			char requesting = clientRequests.charAt(nRequested);
			boolean served = false;	// does the requested data, served?
			
			LOG.info("requests: " + requests);
			do
			{
				boolean inCache = arrayHas(cache, requesting);
				LOG.info(
						step + ". broadcasting: " + broadcasting 
						+ " | requesting: " + requesting
						+ " | inCache: " + inCache
						
				);
				served = (broadcasting == requesting) || inCache;
				
				if (!arrayHas(cache, broadcasting))
				{
					double[] cacheMin = arrayMinAndIndexOfMin(cachePIX);
					
					if (clientRequests.contains(broadcasting + "")	// don't something that will not requested
					&& pix.get(broadcasting) > cacheMin[0])
					{
						LOG.info(
								step + ". cached swaped: " + cache[(int) cacheMin[1]]
								
						);
						cache[(int) cacheMin[1]] = broadcasting;
						cachePIX[(int) cacheMin[1]] = pix.get(broadcasting);
					}
				}
				
				if (served)
				{
					++nRequested;
					requesting = clientRequests.charAt(nRequested);
				}
			} while (served);
			
			++step;
		}
	}

	@Override
	public IsWidget getInputWidget()
	{
		if (pnlBase == null)
		{
			pnlBase = new VerticalPanel();
			((VerticalPanel) pnlBase).setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
			((VerticalPanel) pnlBase).setSpacing(3);
		}
		pnlBase.clear();
		
		String inputExplanationText = "For server schedule and client request," 
				+ "\n" + "every single character (case sensitive) corresponds to a different data."
				+ "\n" + "e.g. for client requests 'ACDDC2a' means 7 requests:"
				+ "\n" + "'A', then 'C', then 'D', then 'D' again, then 'C' again, then '2', then 'a' (it's different from 'A').";
		
		Label lblInputExplanation = new HTML(new SafeHtmlBuilder().appendEscapedLines(inputExplanationText).toSafeHtml());
		
		pnlBase.add(lblInputExplanation);
		
		String serverBroadcastTitle = "server broadcast schedule";
		
		txbServerBroadcastSchedule = new TextBox();
		txbServerBroadcastSchedule.setTitle(serverBroadcastTitle);
		txbServerBroadcastSchedule.getElement().setPropertyString("placeholder", serverBroadcastTitle);
		txbServerBroadcastSchedule.addKeyUpHandler(kuhNodesInputs);
		txbServerBroadcastSchedule.addChangeHandler(cghNodesInputs);
		
		pnlBase.add(txbServerBroadcastSchedule);
		
		String clientRequestsTitle = "client data requests";
		
		txbClientRequests = new TextBox();
		txbClientRequests.setTitle(clientRequestsTitle);
		txbClientRequests.getElement().setPropertyString("placeholder", clientRequestsTitle);
		txbClientRequests.addKeyUpHandler(kuhNodesInputs);
		txbClientRequests.addChangeHandler(cghNodesInputs);
		
		pnlBase.add(txbClientRequests);

		String cacheSlotsTitle = "nubmer of slots";
		
		txbCacheSlots = new TextBox();
		txbCacheSlots.setTitle(cacheSlotsTitle);
		txbCacheSlots.getElement().setPropertyString("placeholder", cacheSlotsTitle);
		txbCacheSlots.getElement().setPropertyString("type", "number");
		txbCacheSlots.getElement().setPropertyInt("min", 1);
		txbCacheSlots.getElement().setPropertyInt("step", 1);
		txbCacheSlots.addKeyUpHandler(kuhNodesInputs);
		txbCacheSlots.addChangeHandler(cghNodesInputs);
		
		pnlBase.add(txbCacheSlots);
		
		return pnlBase;
	}

}
