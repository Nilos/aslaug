package org.uth.aslaug.client.algorithms;

import com.google.gwt.core.client.JavaScriptObject;

public final class VisJsEdge extends JavaScriptObject
{
	protected VisJsEdge() { }
	
	public static native VisJsEdge create(int id, int from, int to, int label)
	/*-{
		return {id: id, from: from, to: to, label: label};
	}-*/;
	
	public native void update(int from, int to, int label)
	/*-{
		this.from = from;
		this.to = to;
		this.label = label;
	}-*/;

	public native int getId()
	/*-{
		return this.id;
	}-*/;

	public native int getFromNodeId()
	/*-{
		return this.from;
	}-*/;

	public native int getToNodeId()
	/*-{
		return this.to;
	}-*/;

	public native int getWeight()
	/*-{
		return this.label;
	}-*/;
}
