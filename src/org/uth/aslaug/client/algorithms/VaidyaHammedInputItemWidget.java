package org.uth.aslaug.client.algorithms;

import org.uth.aslaug.client.Utils;
import com.google.gwt.canvas.dom.client.CssColor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;

public class VaidyaHammedInputItemWidget extends Composite
{
	private int id;
	
	private HorizontalPanel pnlBase;
	
	private TextBox txbLastBroadcast;
	private TextBox txbPopularity;
	private TextBox txbBroadcastLength;
	
	public VaidyaHammedInputItemWidget(int id)
	{
		this.id = id;
		
		CssColor color = Utils.getUniqueColor(this.id);
		
		pnlBase = new HorizontalPanel();
		pnlBase.setVerticalAlignment(HasVerticalAlignment.ALIGN_BOTTOM);
		pnlBase.setSpacing(5);
		pnlBase.getElement().getStyle().setBackgroundColor(color.value());
		
		String nameText = "Disk #" + this.id;
		Label lblName = new Label(nameText);
		pnlBase.add(lblName);
		
		String lastBroadcastTitle = "time of last broadcast [R]";
		txbLastBroadcast = new TextBox();
		txbLastBroadcast.setText("0");
		txbLastBroadcast.setTitle(lastBroadcastTitle);
		txbLastBroadcast.getElement().setPropertyString("placeholder", lastBroadcastTitle);
		txbLastBroadcast.getElement().setPropertyString("type", "number");
		txbLastBroadcast.getElement().setPropertyInt("min", 0);
		txbLastBroadcast.getElement().setPropertyInt("step", 1);
		pnlBase.add(txbLastBroadcast);
		
		String popularityTitle = "popularity [p]";
		txbPopularity = new TextBox();
		txbPopularity.setTitle(popularityTitle);
		txbPopularity.getElement().setPropertyString("placeholder", popularityTitle);
		txbPopularity.getElement().setPropertyString("type", "number");
		txbPopularity.getElement().setPropertyInt("min", 0);
		txbPopularity.getElement().setPropertyInt("step", 1);
		pnlBase.add(txbPopularity);
		
		String broadcastLengthTitle = "lenngth [l]";
		txbBroadcastLength = new TextBox();
		txbBroadcastLength.setTitle(broadcastLengthTitle);
		txbBroadcastLength.getElement().setPropertyString("placeholder", broadcastLengthTitle);
		txbBroadcastLength.getElement().setPropertyString("type", "number");
		txbBroadcastLength.getElement().setPropertyInt("min", 0);
		txbBroadcastLength.getElement().setPropertyInt("step", 1);
		pnlBase.add(txbBroadcastLength);
		
		initWidget(pnlBase);

		// Give the overall composite a style name.
		setStyleName("vaidyaHammedInputItemWidget");
	}
	
	public void setReadOnly(boolean readOnly)
	{
		txbLastBroadcast.setReadOnly(readOnly);
		txbPopularity.setReadOnly(readOnly);
		txbBroadcastLength.setReadOnly(readOnly);
	}

	public int getLastBroadcast()
	{
		return Integer.parseInt(txbLastBroadcast.getText());
	}

	public int getPopularity()
	{
		return Integer.parseInt(txbPopularity.getText());
	}

	public int getBroadcastLength()
	{
		return Integer.parseInt(txbBroadcastLength.getText());
	}	
}
