package org.uth.aslaug.client.algorithms;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.TextBox;

public final class EmptyBaseAlgorithmImpl extends AbstractBaseAlgorithm
{	
	@Override
	public IsWidget getInputWidget()
	{
		TextBox txb = new TextBox();
		txb.setText("");
		txb.setVisible(false);
		
		return txb;
	}
}
