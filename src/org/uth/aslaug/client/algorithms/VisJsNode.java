package org.uth.aslaug.client.algorithms;

import com.google.gwt.core.client.JavaScriptObject;

public final class VisJsNode extends JavaScriptObject
{
	protected VisJsNode() { }
	
	public static native VisJsNode create(int id)
	/*-{
		return {id: id};
	}-*/;

	public native int getId()
	/*-{
		return this.id;
	}-*/;
}
