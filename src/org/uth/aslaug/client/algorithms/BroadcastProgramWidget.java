package org.uth.aslaug.client.algorithms;

import org.uth.aslaug.client.Utils;
import com.google.gwt.canvas.dom.client.CssColor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;

public class BroadcastProgramWidget extends Composite
{
	private HorizontalPanel pnlBase;

	public BroadcastProgramWidget()
	{
		pnlBase = new HorizontalPanel();
		pnlBase.setSpacing(10);
		pnlBase.setBorderWidth(1);
		
		initWidget(pnlBase);

		// Give the overall composite a style name.
		setStyleName("broadcastProgramWidget");
	}
	
	public void addChunk(int diskId, int pageId)
	{
		CssColor color = Utils.getUniqueColor(diskId);
		Label lbl = new Label(pageId + "");
		
		if (pageId <= 0)
		{
			lbl.setText("-");
		}
		HorizontalPanel panel = new HorizontalPanel();
		panel.add(lbl);
		panel.getElement().getStyle().setBackgroundColor(color.value());
		
		pnlBase.add(panel);
	}
}
