package org.uth.aslaug.client.algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class UnbalancedIndexTreeVariableFanoutAlgorithm extends AbstractBaseAlgorithm
{
	private final static Logger LOG = Logger.getLogger(UnbalancedIndexTreeVariableFanoutAlgorithm.class.getName());
	
	private VisJsGraph tree;
	private TextBox txbNodes;
	private List<TreeNodeInputWidget> nodes;

	private final KeyUpHandler kuhNodesInputs = new KeyUpHandler(){
		// TODO: take a look at
		// http://stackoverflow.com/questions/8036561/allow-only-numbers-in-textbox-in-gwt
		@Override
		public void onKeyUp(KeyUpEvent event)
		{
			onNodesInputs();
		}
	};	
	
	private final ChangeHandler cghNodesInputs = new ChangeHandler(){
		
		@Override
		public void onChange(ChangeEvent event)
		{
			onNodesInputs();			
		}
	};
	
	private void onNodesInputs()
	{
		int nNodes = Integer.parseInt(txbNodes.getText());
		nNodes = Math.max(nNodes, 0);	// never trust the user
		
		if (nodes == null)
		{
			nodes = new ArrayList<>();
		}
		
		if (nNodes >= nodes.size()) 
		{
			for (int i = nodes.size(); i < nNodes; i++)
			{
				TreeNodeInputWidget tniw = new TreeNodeInputWidget(i + 1);
				tniw.addKeyUpHandler(kuhNodesInputs);
				tniw.addChangeHandler(cghNodesInputs);

				nodes.add(tniw);
				pnlBase.add(tniw);
			}	
		}
		else 
		{
			int n = nodes.size();
			while (n > nNodes)
			{
				pnlBase.remove(nodes.remove(n - 1));
				--n;
			}
		}
		
		if (nNodes > 0)
		{
			variableFanout();
		}
	}
	
	@Override
	public IsWidget getInputWidget()
	{
		if (pnlBase == null)
		{
			pnlBase = new VerticalPanel();
			((VerticalPanel) pnlBase).setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
			((VerticalPanel) pnlBase).setSpacing(3);
		}
		pnlBase.clear();
		nodes = null;
		
		String nodesTitle = "total nubmer of nodes";
		
		txbNodes = new TextBox();
		txbNodes.setTitle(nodesTitle);
		txbNodes.getElement().setPropertyString("placeholder", nodesTitle);
		txbNodes.getElement().setPropertyString("type", "number");
		txbNodes.getElement().setPropertyInt("min", 0);
		txbNodes.getElement().setPropertyInt("step", 1);
		txbNodes.addKeyUpHandler(kuhNodesInputs);
		txbNodes.addChangeHandler(cghNodesInputs);
		
		pnlBase.add(txbNodes);
		
		Panel panel = new FlowPanel();
		panel.getElement().setId("graph");
		panel.setHeight("350px");

		pnlBase.add(panel);
		
		return pnlBase;
	}
	
	private void variableFanout()
	{
		List<Double> probabilities = new ArrayList<>();
		
		for (TreeNodeInputWidget tnwi : nodes)
		{
			probabilities.add(tnwi.getProbability());
		}
		
		IndexTreeNode<Double> root = new IndexTreeNode<>();
		for (double d : probabilities)
		{
			root.addChildren(new IndexTreeNode<Double>(d));
		}
		
		partition(root);
		indexTreeToJsGraph(root);
		tree.fitGraph(true);
	}
	
	private void partition(IndexTreeNode<Double> node)
	{
		int m = node.getChildrensList().size();
		if (m <= 2)
		{
			LOG.info("too few childrens");
			return;
		}
		
		double[] y = new double[m - 2];

		Collections.sort(node.getChildrensList(), 
				Collections.reverseOrder());	// descending ordes
		
		for (IndexTreeNode<Double> n : node.getChildrensList())
		{
			if (n.getData() == 0.)
			{
				return;
			}
		}
		
		for (int i = 0; i < m - 2; i++)
		{			
			double sum1 = 0;
			double sum2 = 0;
			
			for (int j = 0; j <= i; j++)
			{
				sum1 += node.getChildrensList().get(j).getData();
			}
			
			for (int j = i + 1; j < m; j++)
			{
				sum2 += node.getChildrensList().get(j).getData();
			}
			
			y[i] = (m - 2 - i) * sum1 - sum2;
		}
		LOG.info("input array " + Arrays.toString(node.getChildrensList().toArray()));
		LOG.info("output array " + Arrays.toString(y));
		
		double maxY = y[0];
		int maxYindex = 0;
		
		for (int i = 1; i < m - 2; i++)
		{
			if (y[i] >= maxY)
			{
				maxY = y[i];
				maxYindex = i;
			}
		}
		
		if (maxY <= 0)
		{
			LOG.info("negative or zero max");
			return;
		}
		else 
		{
			LOG.info("max(y)=" + maxY + " | partion on " + (maxYindex + 1) + " child");
		}
		
		IndexTreeNode<Double> h = new IndexTreeNode<>();

		double hSum = 0;
		
		for (int i = maxYindex + 1; i < m; i++)
		{
			IndexTreeNode<Double> itn = node.removeChildren(maxYindex + 1);
			h.addChildren(itn);
			
			hSum += itn.getData();
		}

		partition(h);
		
		h.setData(hSum);
		node.addChildren(h);
		
		partition(node);
	}
	
	private List<VisJsGraph> indexTreeToJsGraph(IndexTreeNode<?> root)
	{		
		if (tree == null)
		{
			tree = new VisJsGraph(0);
		}
		tree.setNodes(0);
		tree.clearEdges();
		
		int n = root.getTotalNodesCount();
		
		LOG.info("total nodes " + n);
		LOG.info("root childs " + root.getChildrensList().size());

		int leafsNodeIndex = 1;
		int parentsNodeIndex = 1;
		List<IndexTreeNode<?>> parents = new ArrayList<>();
		List<Integer> parentsIds = new ArrayList<>();
		parents.add(root);
		
		LOG.info("process childs " + parents.size());

		while (!parents.isEmpty())
		{
			IndexTreeNode<?> parent = parents.remove(0);
			int parentId;
			
			if (parent.getParent() == null)
			{ // root
				parentId = tree.addNode("I");
			}
			else 
			{
				parentId = parentsIds.remove(0);
			}

			for (IndexTreeNode<?> itn : parent.getChildrensList())
			{
				if (itn.getChildrensList().isEmpty())
				{
					int nid = tree.addNode("R" + leafsNodeIndex + "\n" + itn.getData().toString());
					tree.addEdge(parentId, nid);
					++leafsNodeIndex;
				}
				else
				{
					int nid = tree.addNode("a" + parentsNodeIndex + "\n" + itn.getData().toString());
					tree.addEdge(parentId, nid);
					parents.add(itn);
					parentsIds.add(nid);
					++parentsNodeIndex;
				}
			}
		}
		
		// TODO: change logic so it will return every subtree of running instances
		return null;
	}
	
	static class NodeData implements Comparable<NodeData>
	{
		private double probability;
		private String name;
		
		public NodeData(double probability, String name)
		{
			super();
			this.probability = probability;
			this.name = name;
		}

		public double getProbability()
		{
			return probability;
		}

		public void setProbability(double probability)
		{
			this.probability = probability;
		}

		public String getName()
		{
			return name;
		}

		public void setName(String name)
		{
			this.name = name;
		}

		@Override
		public int compareTo(NodeData o)
		{
			return Double.compare(getProbability(), o.getProbability());
		}
	}
}
