package org.uth.aslaug.client.algorithms;

import java.util.ArrayList;
import java.util.List;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class RXWBroadcastAlgorithm extends AbstractBaseAlgorithm
{
	private static final String STEP_DELIMETER = "\n----------\n";

	private List<RXWInputItemWidget> lInputs;
	
	private Timer timer;
	
	private Button btnNextStep;
	private Button btnReset;
	private Button btnStartPause;
	private Button btnAddDisk;
	private Button btnRemoveDisk;
	
	private TextBox txbTime;
	
	private TextArea txaStepsLog;
	
	private int time;
	
	private int[] requests;
	private int[] waitTimes;
	private int[] maxReqPerTick;
	
	public RXWBroadcastAlgorithm()
	{
		lInputs = new ArrayList<>();
	}
	
	private final ClickHandler chReset = new ClickHandler(){
		
		@Override
		public void onClick(ClickEvent event)
		{
			timer.cancel();
			timer = null;	// used to determine state of start/pause
			getInputWidget();
		}
	};
	
	private final ClickHandler chAddDisk = new ClickHandler(){
		
		@Override
		public void onClick(ClickEvent event)
		{
			RXWInputItemWidget rxwiiw = new RXWInputItemWidget(lInputs.size() + 1);
			lInputs.add(rxwiiw);
			((VerticalPanel) pnlBase).insert(rxwiiw, lInputs.size());			
		}
	};
	
	private final ClickHandler chRemoveDisk = new ClickHandler(){
		
		@Override
		public void onClick(ClickEvent event)
		{
			RXWInputItemWidget rxwiiw = lInputs.remove(lInputs.size() - 1);
			((VerticalPanel) pnlBase).remove(rxwiiw);
		}
	};
	
	private final ClickHandler chStartPause = new ClickHandler(){
		
		@Override
		public void onClick(ClickEvent event)
		{
			String pauseText = "Pause";
			
			if (!pauseText.equals(btnStartPause.getText()))
			{
				if (timer == null)
				{
					requests = new int[lInputs.size()];
					waitTimes = new int[lInputs.size()];
					maxReqPerTick = new int[lInputs.size()];
					
					int i = 0;
					for (RXWInputItemWidget rxwiiw : lInputs)
					{
						requests[i] = rxwiiw.getRequests();
						waitTimes[i] = rxwiiw.getWaitTime();
						maxReqPerTick[i] = rxwiiw.getMaxRequestsPerTick();
								
						rxwiiw.setReadOnly(true);
						
						++i;
					}
					
					time = Integer.parseInt(txbTime.getText());
					
					((ComplexPanel) pnlBase).remove(lInputs.size() + 1);
					
					txaStepsLog = new TextArea();
					txaStepsLog.setReadOnly(true);
					txaStepsLog.setSize("450px", "300px");
					pnlBase.add(txaStepsLog);
					
					appendToStepsLog("Initial inputs:", true);
					appendToStepsLog("Time: " + time, true);
					appendToStepsLog("Disks: " + lInputs.size(), true);			
					
					int n = lInputs.size();
					for (int j = 0; j < n; j++)
					{
						int rxw = requests[j] * waitTimes[j];
						
						appendToStepsLog("\tDisk " + (j + 1) + " | R x W = " + rxw, true);
						appendToStepsLog("\t\tRequests [R]: " + requests[j], true);
						appendToStepsLog("\t\tWait time [W]: " + waitTimes[j], true);
					}
					appendToStepsLog(STEP_DELIMETER, true);
					
					String nextStepText = "Next step";
					btnNextStep = new Button();
					btnNextStep.setText(nextStepText);
					btnNextStep.addClickHandler(chNextStep);
					pnlBase.add(btnNextStep);
					
					timer = new Timer(){
						
						@Override
						public void run()
						{
							btnNextStep.click();					
						}
					};
					
					String resetText = "Reset";
					btnReset = new Button();
					btnReset.setText(resetText);
					btnReset.addClickHandler(chReset);
					pnlBase.add(btnReset);
				}
				btnStartPause.setText(pauseText);
				timer.scheduleRepeating(2500);
			}
			else 
			{
				String startText = "Start";
				btnStartPause.setText(startText);
				timer.cancel();
			}
		}
	};
	
	private final ClickHandler chNextStep = new ClickHandler(){
		
		@Override
		public void onClick(ClickEvent event)
		{
			int maxRXW = requests[0] * waitTimes[0];
			int indexOfMax = 0;
			
			int n = lInputs.size();
			
			for (int i = 1; i < n; i++)
			{
				int rxw = requests[i] * waitTimes[i];
				
				if (rxw > maxRXW)
				{
					maxRXW = rxw;
					indexOfMax = i;
				}
			}
			
			appendToStepsLog("Time: " + time 
							+ " Braodcasts: max R x W = R x W (" 
							+ (indexOfMax + 1) + ") = " + maxRXW, true);
			
			++time;
			waitTimes[indexOfMax] = 0;
			requests[indexOfMax] = 0;
			
			for (int j = 0; j < n; j++)
			{
				int rand = Random.nextInt(maxReqPerTick[j] + 1);
				requests[j] += rand;
				++waitTimes[j];
				int rxw = requests[j] * waitTimes[j];
				
				appendToStepsLog("\tDisk " + (j + 1) + " | R x W = " + rxw, true);
				appendToStepsLog("\t\tRequests [R]: " + requests[j] + " (" + rand + " new)", true);
				appendToStepsLog("\t\tWait time [W]: " + waitTimes[j], true);
			}
			appendToStepsLog(STEP_DELIMETER, true);	
			
			txaStepsLog.setCursorPos(txaStepsLog.getText().length() - 1);
		}
	};
	
	private void appendToStepsLog(String text, boolean appendNewLine)
	{
		txaStepsLog.setText(txaStepsLog.getText() + text);
		if (appendNewLine)
		{
			txaStepsLog.setText(txaStepsLog.getText() + "\n");
		}
	}
	
	@Override
	public IsWidget getInputWidget()
	{
		if (pnlBase == null)
		{
			pnlBase = new VerticalPanel();
			((VerticalPanel) pnlBase).setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
			((VerticalPanel) pnlBase).setSpacing(3);
		}
		pnlBase.clear();
		lInputs.clear();
		
		HorizontalPanel hp = new HorizontalPanel();
		hp.setSpacing(5);
		pnlBase.add(hp);
		
		String timeTitle = "current time";
		Label lblTime = new Label(timeTitle + ": ");
		hp.add(lblTime);
		
		lInputs.add(new RXWInputItemWidget(lInputs.size() + 1));
		pnlBase.add(lInputs.get(0));
		
		txbTime = new TextBox();
		txbTime.setText("0");
		txbTime.setTitle(timeTitle);
		txbTime.getElement().setPropertyString("placeholder", timeTitle);
		txbTime.getElement().setPropertyString("type", "number");
		txbTime.getElement().setPropertyInt("min", 0);
		txbTime.getElement().setPropertyInt("step", 1);
		hp.add(txbTime);
		
		hp = new HorizontalPanel();
		hp.setSpacing(8);
		pnlBase.add(hp);
		
		String addDiskText = "Add disk";
		btnAddDisk = new Button();
		btnAddDisk.setText(addDiskText);
		btnAddDisk.addClickHandler(chAddDisk);
		hp.add(btnAddDisk);
		
		String removeDiskText = "Remove disk";
		btnRemoveDisk = new Button();
		btnRemoveDisk.setText(removeDiskText);
		btnRemoveDisk.addClickHandler(chRemoveDisk);
		hp.add(btnRemoveDisk);
		
		String startText = "Start";
		btnStartPause = new Button(startText);
		btnStartPause.addClickHandler(chStartPause);
		pnlBase.add(btnStartPause);
		
		return pnlBase;
	}
}
