package org.uth.aslaug.client.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IndexedPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class HeuristicMSTAlgorithm extends AbstractBaseAlgorithm
{
	private final static Logger LOG = Logger.getLogger(HeuristicMSTAlgorithm.class.getName());
	
	private VisJsGraph graph;
	private List<GraphEdgeInputWidget> edges;
	
	private TextBox txbNodes;
	private Button btnAddEdge;
	private Button btnRemoveEdge;
	private Button btnDone;
	
	private final ClickHandler chAddEdge = new ClickHandler(){
		
		@Override
		public void onClick(ClickEvent event)
		{
			newEdgeInput();
		}
	};
	
	private final ClickHandler chRemoveEdge = new ClickHandler(){
		
		@Override
		public void onClick(ClickEvent event)
		{
			removeEdgeInput();
		}
	};
	
	private final ClickHandler chDone = new ClickHandler(){
		
		@Override
		public void onClick(ClickEvent event)
		{
			String disksInputDoneText = "Reset";
			
			if (disksInputDoneText.equals(btnDone.getText()))
			{
				LOG.info("inputs reset");
				
				getInputWidget();
			}
			else
			{
				LOG.info("inputs done");
				
				for (GraphEdgeInputWidget geiw : edges)
				{
					if (!geiw.hasValidInput())
					{
						return;
					}
				}
				
				txbNodes.setReadOnly(true);
				for (GraphEdgeInputWidget geiw : edges)
				{
					geiw.setEnabled(false);
				}
		
				btnDone.setText(disksInputDoneText);
				pnlBase.remove(btnAddEdge.getParent());
				
				List<ArrayList<VisJsNode>> lPartitions = new ArrayList<>();
				int n = graph.getNodesCount();
				for (int i = 1; i <= n; i++)
				{
					ArrayList<VisJsNode> e = new ArrayList<>();
					e.add(VisJsNode.create(i));
					
					lPartitions.add(e);
				}

				JsArray<VisJsEdge> e = graph.getEdges();				
				List<VisJsEdge> lEdges = new ArrayList<>();
				n = e.length();
				for (int i = 0; i < n; i++)
				{
					VisJsEdge vje = e.get(i);
					lEdges.add(vje);
				}
				
//				List<VisJsNode> mst = new ArrayList<>();
				
				// sort edges in non-increasing order
				Collections.sort(lEdges, Collections.reverseOrder(new VisJsEdgeComparator()));

				// for each edge(u, v)
				for (VisJsEdge vje : lEdges)
				{
					ArrayList<VisJsNode> pOfU = lPartitions.get(0);
					ArrayList<VisJsNode> pOfV = lPartitions.get(0);
					
					LOG.info("checks edge " + vje.getFromNodeId() + "->" + vje.getToNodeId());
					
					if (vje.getFromNodeId() != vje.getToNodeId())
					{
						for (ArrayList<VisJsNode> lvjn : lPartitions)
						{// finds partition of U and V

							for (VisJsNode vjn : lvjn)
							{
								if (vjn.getId() == vje.getFromNodeId())
								{
									pOfU = lvjn;
									break;
								}
							}

							for (VisJsNode vjn : lvjn)
							{
								if (vjn.getId() == vje.getToNodeId())
								{
									pOfV = lvjn;
									break;
								}
							}
						}

						if (pOfU != pOfV)
						{
							LOG.info("adds edge " + vje.getId());
							
							graph.highlightEdge(vje.getId());

							lPartitions.remove(pOfV);
							pOfU.addAll(pOfV);
						}
					}
				}
				
				for (ArrayList<VisJsNode> lvjn : lPartitions)
				{
					for (VisJsNode vjn : lvjn)
					{
						LOG.info("broadcast node " + vjn.getId());
					}
				}	
				
				// TODO: add texts to show the order o selected nodes
			}
		}
	};
	
	private final KeyUpHandler kuhNodesInputs = new KeyUpHandler(){
		// TODO: take a look at
		// http://stackoverflow.com/questions/8036561/allow-only-numbers-in-textbox-in-gwt
		@Override
		public void onKeyUp(KeyUpEvent event)
		{
			onNodesInputs();
		}
	};	
	
	private final ChangeHandler cghNodesInputs = new ChangeHandler(){
		
		@Override
		public void onChange(ChangeEvent event)
		{
			onNodesInputs();			
		}
	};
	
	private void onNodesInputs()
	{
		int nodes = Integer.parseInt(txbNodes.getText());
		
		if (graph != null)
		{
			graph.setNodes(nodes);
			
			for (GraphEdgeInputWidget geiw : edges)
			{
				geiw.updateEdges();
			}
		}
		else 
		{
			graph = new VisJsGraph(nodes);
			edges = new ArrayList<>();
			newEdgeInput();
		}		
	}
	
	@Override
	public IsWidget getInputWidget()
	{
		if (pnlBase == null)
		{
			pnlBase = new VerticalPanel();
			((VerticalPanel) pnlBase).setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
			((VerticalPanel) pnlBase).setSpacing(3);
		}
		pnlBase.clear();
		graph = null;
		edges = null;
		
		String nodesTitle = "total nubmer of nodes";
		
		txbNodes = new TextBox();
		txbNodes.setTitle(nodesTitle);
		txbNodes.getElement().setPropertyString("placeholder", nodesTitle);
		txbNodes.getElement().setPropertyString("type", "number");
		txbNodes.getElement().setPropertyInt("min", 0);
		txbNodes.getElement().setPropertyInt("step", 1);
		txbNodes.addKeyUpHandler(kuhNodesInputs);
		txbNodes.addChangeHandler(cghNodesInputs);
		
		pnlBase.add(txbNodes);
		
		Panel panel = new FlowPanel();
		panel.getElement().setId("graph");
		panel.setHeight("350px");

		pnlBase.add(panel);
		
		HorizontalPanel hp = new HorizontalPanel();
		hp.setSpacing(8);
		pnlBase.add(hp);
		
		String addEdgeText = "Add edge";
		btnAddEdge = new Button();
		btnAddEdge.setText(addEdgeText);
		btnAddEdge.addClickHandler(chAddEdge);
		hp.add(btnAddEdge);
		
		String removeEdgeText = "Remove edge";
		btnRemoveEdge = new Button();
		btnRemoveEdge.setText(removeEdgeText);
		btnRemoveEdge.addClickHandler(chRemoveEdge);
		hp.add(btnRemoveEdge);
		
		String doneText = "Done";
		btnDone = new Button(doneText);
		btnDone.addClickHandler(chDone);
		pnlBase.add(btnDone);
				
		return pnlBase;
	}
	
	private void newEdgeInput()
	{
		GraphEdgeInputWidget geiw = new GraphEdgeInputWidget(graph);
		edges.add(geiw);
		
		((VerticalPanel) pnlBase).insert(geiw, ((IndexedPanel) pnlBase).getWidgetCount() - 2);
	}
	
	private void removeEdgeInput(GraphEdgeInputWidget geiw)
	{
		edges.remove(geiw);
		pnlBase.remove(geiw);
	}
	
	private void removeEdgeInput()
	{
		graph.removeEdge();
		removeEdgeInput(edges.get(edges.size() - 1));
	}
}
