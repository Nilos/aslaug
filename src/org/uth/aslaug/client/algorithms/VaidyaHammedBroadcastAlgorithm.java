package org.uth.aslaug.client.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class VaidyaHammedBroadcastAlgorithm extends AbstractBaseAlgorithm
{
	private final static Logger LOG = Logger.getLogger(VaidyaHammedBroadcastAlgorithm.class.getName());
	private static final String STEP_DELIMETER = "\n----------\n";
	
	private List<VaidyaHammedInputItemWidget> lInputs;
	
	private Button btnAddDisk;
	private Button btnRemoveDisk;
	private Button btnDone;
	private Button btnNextStep;
	
	private TextBox txbTime;
	private TextArea txaStepsLog;
	
	private int steps;
	private int time;
	/** time of last broadcast of each disk */
	private int[] lastBroadcast;
	/** G value of each disk */
	private List<Double> gVals;
	
	public VaidyaHammedBroadcastAlgorithm()
	{
		lInputs = new ArrayList<>();
	}
	
	private final ClickHandler chAddDisk = new ClickHandler(){
		
		@Override
		public void onClick(ClickEvent event)
		{
			VaidyaHammedInputItemWidget vhiiw = new VaidyaHammedInputItemWidget(lInputs.size() + 1);
			lInputs.add(vhiiw);
			((VerticalPanel) pnlBase).insert(vhiiw, lInputs.size());			
		}
	};
	
	private final ClickHandler chRemoveDisk = new ClickHandler(){
		
		@Override
		public void onClick(ClickEvent event)
		{
			VaidyaHammedInputItemWidget vhiiw = lInputs.remove(lInputs.size() - 1);
			((VerticalPanel) pnlBase).remove(vhiiw);
		}
	};
	
	private final ClickHandler chDone = new ClickHandler(){
		
		@Override
		public void onClick(ClickEvent event)
		{
			String disksInputDoneText = "Reset";
			
			if (disksInputDoneText.equals(btnDone.getText()))
			{
				LOG.info("disk inputs reset");
				
				lInputs.clear();
				getInputWidget();
			}
			else
			{
				LOG.info("disk inputs done");
				
				lastBroadcast = new int[lInputs.size()];
				int i = 0;
				for (VaidyaHammedInputItemWidget vhiiw : lInputs)
				{
					vhiiw.getBroadcastLength();
					vhiiw.getPopularity();
					lastBroadcast[i++] = vhiiw.getLastBroadcast();
				}
				time = Integer.parseInt(txbTime.getText());
				steps = 1;
				
				txbTime.setReadOnly(true);
				for (VaidyaHammedInputItemWidget vhiiw : lInputs)
				{
					vhiiw.setReadOnly(true);
				}
				((ComplexPanel) pnlBase).remove(lInputs.size() + 1);
				
				btnDone.setText(disksInputDoneText);
				
				txaStepsLog = new TextArea();
				txaStepsLog.setReadOnly(true);
				txaStepsLog.setSize("450px", "300px");
				pnlBase.add(txaStepsLog);
				
				appendToStepsLog("Initial inputs:", true);
				appendToStepsLog("Time [Q]: " + time, true);
				appendToStepsLog("Disks: " + lInputs.size(), true);
				
				gVals = new ArrayList<>();
				int n = lInputs.size();
				for (int j = 1; j <= n; j++)
				{
					int R = lInputs.get(j - 1).getLastBroadcast();
					int p = lInputs.get(j - 1).getPopularity();
					int l = lInputs.get(j - 1).getBroadcastLength();
					double g = getGval(time, R, p, l);
					
					gVals.add(g);					
					
					appendToStepsLog("\tDisk " + j + " | G = " + g, true);
					appendToStepsLog("\t\tPopularity [p]: " + p, true);
					appendToStepsLog("\t\tBroadcast Length [l]: " + l, true);
					appendToStepsLog("\t\tLast Broadcast Time [R]: " + R, true);
				}
				appendToStepsLog(STEP_DELIMETER, true);
				
				String nextStepText = "Next step";
				btnNextStep = new Button();
				btnNextStep.setText(nextStepText);
				btnNextStep.addClickHandler(chNextStep);
				pnlBase.add(btnNextStep);
			}
		}
	};
	
	private final ClickHandler chNextStep = new ClickHandler(){
		
		@Override
		public void onClick(ClickEvent event)
		{
			double maxG = Collections.max(gVals);
			int i = gVals.indexOf(maxG);
			
			appendToStepsLog("Time: " + time 
							+ " | Step " + steps 
							+ " Braodcasts: max G = G(" + (i + 1) + ") = " + maxG, true);
			lastBroadcast[i] = time;
			time += lInputs.get(i).getBroadcastLength();
			
			int n = lInputs.size();
			for (int j = 1; j <= n; j++)
			{
				int R = lastBroadcast[j - 1];
				int p = lInputs.get(j - 1).getPopularity();
				int l = lInputs.get(j - 1).getBroadcastLength();
				double g = getGval(time, R, p, l);
				
				gVals.set(j - 1, g);					
				
				appendToStepsLog("\tDisk " + j + " | G = " + g, true);
				appendToStepsLog("\t\tPopularity [p]: " + p, true);
				appendToStepsLog("\t\tBroadcast Length [l]: " + l, true);
				appendToStepsLog("\t\tLast Broadcast Time [R]: " + R, true);
			}
			appendToStepsLog(STEP_DELIMETER, true);	
			
			txaStepsLog.setCursorPos(txaStepsLog.getText().length() - 1);
			
			++steps;
		}
	};
	
	private void appendToStepsLog(String text, boolean appendNewLine)
	{
		txaStepsLog.setText(txaStepsLog.getText() + text);
		if (appendNewLine)
		{
			txaStepsLog.setText(txaStepsLog.getText() + "\n");
		}
	}
	
	@Override
	public IsWidget getInputWidget()
	{
		if (pnlBase == null)
		{
			pnlBase = new VerticalPanel();
			((VerticalPanel) pnlBase).setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
			((VerticalPanel) pnlBase).setSpacing(3);
		}
		pnlBase.clear();
		lInputs.clear();
		
		HorizontalPanel hp = new HorizontalPanel();
		hp.setVerticalAlignment(HasVerticalAlignment.ALIGN_BOTTOM);
		hp.setSpacing(3);
		pnlBase.add(hp);
		
		String timeTitle = "current time";
		Label lblTime = new Label(timeTitle + ": ");
		hp.add(lblTime);
		
		txbTime = new TextBox();
		txbTime.setText("0");
		txbTime.setTitle(timeTitle);
		txbTime.getElement().setPropertyString("placeholder", timeTitle);
		txbTime.getElement().setPropertyString("type", "number");
		txbTime.getElement().setPropertyInt("min", 0);
		txbTime.getElement().setPropertyInt("step", 1);
		hp.add(txbTime);
		
		lInputs.add(new VaidyaHammedInputItemWidget(lInputs.size() + 1));
		pnlBase.add(lInputs.get(0));
		
		hp = new HorizontalPanel();
		hp.setSpacing(8);
		pnlBase.add(hp);
		
		String addDiskText = "Add disk";
		btnAddDisk = new Button();
		btnAddDisk.setText(addDiskText);
		btnAddDisk.addClickHandler(chAddDisk);
		hp.add(btnAddDisk);
		
		String removeDiskText = "Remove disk";
		btnRemoveDisk = new Button();
		btnRemoveDisk.setText(removeDiskText);
		btnRemoveDisk.addClickHandler(chRemoveDisk);
		hp.add(btnRemoveDisk);
		
		String doneText = "Done";
		btnDone = new Button(doneText);
		btnDone.addClickHandler(chDone);
		pnlBase.add(btnDone);
		
		return pnlBase;
	}
	
	private static double getGval(int Q, int R, int p, int l)
	{
		int n = Q - R;
		
		return n * n * (p / (double) l);
	}
}
