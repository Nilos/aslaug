package org.uth.aslaug.client;

import org.uth.aslaug.client.algorithms.AbstractBaseAlgorithm;
import org.uth.aslaug.client.algorithms.BraodcastDisksAlgorithm;
import org.uth.aslaug.client.algorithms.EmptyBaseAlgorithmImpl;
import org.uth.aslaug.client.algorithms.HeuristicMSTAlgorithm;
import org.uth.aslaug.client.algorithms.ProbabilityInverseBroadcactFrequencyAlgorithm;
import org.uth.aslaug.client.algorithms.RXWBroadcastAlgorithm;
import org.uth.aslaug.client.algorithms.UnbalancedIndexTreeVariableFanoutAlgorithm;
import org.uth.aslaug.client.algorithms.VaidyaHammedBroadcastAlgorithm;

public final class Algorithms
{
	public static final String[] NAMES = 
			{
			 	"",
				"Broadcast Disks",
				"Αλγόρ. εκπομπής Vaidya-Hameed",
				"RxW (Συνδυασμός MRF & FCFS)",
				"Probability Inv. Broadc. Frequency",
				"Ο ευριστικός αλγόριθμος MST",
				"Μη ισοζυγισμένα δένδρα μεταβλητού fanout"
			};
	
	public static final AbstractBaseAlgorithm[] CLASSES = new AbstractBaseAlgorithm[]			
			{
			 	new EmptyBaseAlgorithmImpl(),
			 	new BraodcastDisksAlgorithm(),
			 	new VaidyaHammedBroadcastAlgorithm(),
			 	new RXWBroadcastAlgorithm(),
			 	new ProbabilityInverseBroadcactFrequencyAlgorithm(),
			 	new HeuristicMSTAlgorithm(),
			 	new UnbalancedIndexTreeVariableFanoutAlgorithm(),
			};
}