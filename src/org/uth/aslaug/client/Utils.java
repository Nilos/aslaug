package org.uth.aslaug.client;

import com.google.gwt.canvas.dom.client.CssColor;

/**
 * 
 * gcd and lcm from:
 * http://stackoverflow.com/questions/4201860/how-to-find-gcf-lcm-on-a-set-of-numbers
 *
 * rgbToHex from:
 * http://stackoverflow.com/questions/15403471/use-color-class-in-gwt
 */
public final class Utils
{
	public static long gcd(long a, long b)
	{
	    while (b > 0)
	    {
	        long temp = b;
	        b = a % b;
	        a = temp;
	    }
	    return a;
	}

	public static long gcd(long[] input)
	{
	    long result = input[0];
	    for(int i = 1; i < input.length; i++) result = gcd(result, input[i]);
	    return result;
	}
	
	public static long lcm(long a, long b)
	{
	    return a * (b / gcd(a, b));
	}

	public static long lcm(long[] input)
	{
	    long result = input[0];
	    for(int i = 1; i < input.length; i++) result = lcm(result, input[i]);
	    return result;
	}
	
	public static String rgbToHex(int r, int g, int b)
	{
		StringBuilder sb = new StringBuilder();
		sb.append('#')
		.append(Integer.toHexString(r))
		.append(Integer.toHexString(g))
		.append(Integer.toHexString(b));
		
		return sb.toString();
	}
	
	// TODO: also look at http://phrogz.net/css/distinct-colors.html
	public static CssColor getUniqueColor(int id)
	{
		final int maxColors = 6;
		int r, g, b;
		r = g = b = 255;
		
		switch (id % maxColors)
		{
		case 0:	// red
			g = b = 0;
			break;
			
		case 1:	// green
			r = b = 0;
			break;
			
		case 2:	// blue
			r = g = 0;
			break;
		
		case 3:	// yellow
			b = 0;
			break;
		
		case 4:	// fuchsia
			g = 0;
			break;
		
		case 5:	// aqua
			r = 0;
			break;
		}
		
		if (id > maxColors)
		{
			int step = 96;
						
			r = Math.max(0, Math.min(255,  r + step * (id / maxColors)));
			g = Math.max(0, Math.min(255,  g + step * (id / maxColors)));
			b = Math.max(0, Math.min(255,  b + step * (id / maxColors)));
		}
		
		return CssColor.make(r, g,	b);
	}
	
	public static String asHTMLFraction(int a, int b)
	{
		return "<sup>" + a + "</sup>&frasl;<sub>" + b + "</sub>";
	}

	public static boolean arrayHas(char[] array, char item)
	{
		for (char c : array)
		{
			if (c == item)
			{
				return true;
			}
		}
		
		return false;
	}
	
	public static double[] arrayMinAndIndexOfMin(double[] a)
	{
		double[] r = new double[2];
		
		r[0] = a[0];
		r[1] = 0;
		
		if (a.length > 1)
		{
			for (int i = 1; i < a.length; i++)
			{
				if (a[i] < r[0])
				{
					r[0] = a[i];
					r[1] = i;
				}
			}
		}
		
		return r;
	}
}
