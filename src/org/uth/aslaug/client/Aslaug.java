package org.uth.aslaug.client;

import java.util.logging.Logger;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Aslaug implements EntryPoint 
{
	private final static Logger LOG = Logger.getLogger(Aslaug.class.getName());

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() 
	{
		final ListBox lsbAlgorithmNames = new ListBox();

		for (String name : Algorithms.NAMES)
		{
			lsbAlgorithmNames.addItem(name);
		}
		
		lsbAlgorithmNames.addChangeHandler(new ChangeHandler(){
			
			@Override
			public void onChange(ChangeEvent event)
			{
				int i = lsbAlgorithmNames.getSelectedIndex();
				
				LOG.info("Selected algorithm " + i + " [" + Algorithms.NAMES[i] + "]");
				LOG.info("Selected class " + i + " [" + Algorithms.CLASSES[i].getClass().getName() + "]");
				
				Panel panel = RootPanel.get("inputFieldsContainer");
				panel.clear();
				
				panel.add(Algorithms.CLASSES[i].getInputWidget());
			}
		});
		
		RootPanel.get("algorithmNameFieldContainer").add(lsbAlgorithmNames);

		// Focus the cursor on the name field when the app loads
		lsbAlgorithmNames.setFocus(true);
	}
}
