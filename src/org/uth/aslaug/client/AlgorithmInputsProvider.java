package org.uth.aslaug.client;

import com.google.gwt.user.client.ui.IsWidget;

public interface AlgorithmInputsProvider
{
	public IsWidget getInputWidget();
}
